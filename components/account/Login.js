import React, {useState} from 'react'
import LoginImg from '../../assets/images/loginimg.png';
import Image from 'next/image';
import { useRouter } from 'next/router';
import { 
    AtSymbolIcon, 
    LockClosedIcon, 
    ArrowRightCircleIcon, 
    ArrowLongRightIcon,
    UserCircleIcon
} from '@heroicons/react/24/outline';

function Login() {
    const router = useRouter();
    const [ifRegister, setIfRegister] = useState(false);

    const handleRegister = () => {
        if (!ifRegister) {
            setIfRegister(true);
        }else{
            setIfRegister(false);
        }
    }

    const handleSignIn = () => {
        router.push('/dashboard')
    }

    return ( 
        <div className='flex min-w-full min-h-screen p-5 bg-white justify-center'>
            <div className='bg-orange-500 grid grid-cols-2 gap-2 justify-center'>
                <div className='flex h-full items-center'>
                    <Image src={LoginImg} height={300} width={500} atl='Login img' className=''/>
                </div>
                <div className='flex relative p-5 items-center justify-center border-white border-l-4'>
                    <ArrowRightCircleIcon className=' w-20 h-20 rounded-full bg-white text-orange-500 shadow-md shadow-orange-800 font-bold absolute top-auto -left-10' />
                    <form className='flex flex-col items-center'>
                        { 
                            ifRegister === false ? <> 
                                <span className=' text-3xl mb-2 text-white font-bold font-sans'> Explore </span>
                                <span className=' text-sm mb-20 text-white text-center font-bold font-sans'>Lorem Ipsum Admin Task with Kanban and Gant</span>
                                <div className='flex flex-row rounded bg-white p-1 mb-5'>
                                    <input  type='text' className='h-7  rounded text-xl ml-3 bg-transparent border-none outline-none text-orange-500 placeholder:text-orange-300' placeholder='email@email.com' />
                                    <AtSymbolIcon className='w-7 h-7 text-orange-300' />
                                </div>
                                <div className='flex flex-row rounded bg-white p-1 mb-5'>
                                    <input  type='text' className='h-7  rounded text-xl ml-3 bg-transparent border-none outline-none text-orange-500 placeholder:text-orange-300' placeholder='password' />
                                    <LockClosedIcon className='w-7 h-7 text-orange-300' />
                                </div>
                            </> : <>
                                <span className=' text-3xl mb-2 text-white font-bold font-sans'> Welcome </span>
                                <span className=' text-sm mb-20 text-white text-center font-bold font-sans'>Lorem Ipsum Admin Task with Kanban and Gant</span>
                                <div className='flex flex-row rounded bg-white p-1 mb-5'>
                                    <input  type='text' className='h-7  rounded text-xl ml-3 bg-transparent border-none outline-none text-orange-500 placeholder:text-orange-300' placeholder='First Name' />
                                    <UserCircleIcon className='w-7 h-7 text-orange-300' />
                                </div>
                                <div className='flex flex-row rounded bg-white p-1 mb-5'>
                                    <input  type='text' className='h-7  rounded text-xl ml-3 bg-transparent border-none outline-none text-orange-500 placeholder:text-orange-300' placeholder='email@email.com' />
                                    <AtSymbolIcon className='w-7 h-7 text-orange-300' />
                                </div>
                                <div className='flex flex-row rounded bg-white p-1 mb-5'>
                                    <input  type='text' className='h-7  rounded text-xl ml-3 bg-transparent border-none outline-none text-orange-500 placeholder:text-orange-300' placeholder='password' />
                                    <LockClosedIcon className='w-7 h-7 text-orange-300' />
                                </div>
                            </>
                        }
                        <button  type='button' className='flex mb-2 hover:bg-orange-100 hover:text-orange-600 bg-white rounded shadow-sm text-orange-300 space-x-2 w-40 h-9 p-1 items-center justify-center'
                            onClick={handleSignIn}>
                            <span>{ifRegister === false? 'Sign In':'Sign Up'}</span>
                            <ArrowLongRightIcon className='w-7 h-7 '/>
                        </button>
                        <a href='#register' onClick={handleRegister} className=' text-xs text-white'>{ifRegister === false? 'Sign Up':'Sign In'}</a>
                    </form>
                </div>
            </div>
        </div>
     );
}

export default Login;