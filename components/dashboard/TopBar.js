import React from 'react';
import { 
    MagnifyingGlassCircleIcon,
    BellAlertIcon,
    AtSymbolIcon
} from '@heroicons/react/24/outline';
import Image from 'next/image';

function TobBar() {
    return ( 
        <div className='h-16 pl-40 fixed bg-gradient-to-r
        from-orange-400 to-orange-500 w-full flex items-center justify-between pr-5'>
            <div className='flex px-5 ml-10 w-80 p-2 space-x-5 border-b-2 border-b-white'>
                <input type="text" placeholder='Search for task ...' 
                className='bg-transparent w-60 text-white placeholder-white 
                border-none outline-0 focus:ring-0 text-lg font-semibold'/>
                <button type="button" className=''>
                    <MagnifyingGlassCircleIcon className='h-7 w-7 text-white font-bold' />
                </button>
            </div>
            <div className='flex space-x-6'>
                <AtSymbolIcon className='w-7 h-7 text-white font-bold'/>
                <BellAlertIcon className='w-7 h-7 text-white font-bold'/>
                <a href="#"><div className='flex items-center justify-center space-x-4 text-white font-bold'>
                    <h3 className='mr-5'>Mr. Herrera & H</h3>
                    <Image width={"40"} height={"40"} src={"https://randomuser.me/api/portraits/men/40.jpg"} 
                    className="rounded-full" />
                </div></a>
            </div>
        </div>
    );
}

export default TobBar;