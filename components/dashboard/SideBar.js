import React from 'react';
import { 
    UserGroupIcon, 
    CircleStackIcon, 
    CalendarIcon, 
    ChartBarIcon,
    CogIcon
} from '@heroicons/react/24/outline';

function SideBar() {
    return ( 
        <div className='fixed inset-y-0 left-0 bg-white w-40'>
            <h1 className=' flex items-center justify-center text-2xl h-16 
            bg-orange-500 text-white font-bold'>
                H&HKAN
            </h1>

            <ul className='flex flex-col text-lg
             h-full' >
                <li className='flex justify-center items-center flex-col 
                py-5 text-gray-500'>
                    <UserGroupIcon className='w-7 h7'></UserGroupIcon>
                    Manage
                </li>
                <li className='flex justify-center items-center 
                flex-col py-5 text-orange-500 border-orange-500 border-l-4
                font-bold'>
                    <CircleStackIcon className='w-7 h7 text-orange-500'/>
                    Boards
                </li>
                <li className='flex justify-center items-center 
                flex-col py-5 text-gray-500'>
                    <CalendarIcon className='w-7 h7'/>
                    Schedule
                </li>
                <li className='flex justify-center items-center 
                flex-col py-5 text-gray-500'>
                    <ChartBarIcon className='w-7 h7'/>
                    Report
                </li>
                <li className='flex justify-center items-center 
                flex-col py-5 text-gray-500 m-auto mb-14'>
                    <CogIcon className='w-7 h7'/>
                    Settings
                </li>
            </ul>
        </div> 
    );
}

export default SideBar;