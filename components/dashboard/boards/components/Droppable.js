import React, {useState, useEffect} from 'react';

import {
    EllipsisVerticalIcon,
    PlusCircleIcon
} from '@heroicons/react/24/outline';
import CardItem from './CardItems';
import BoardData from '../../../../data/board-data.json';
import { Droppable } from "react-beautiful-dnd";

function createGuidId() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}

function DroppableAnimation() {
    const [ enabled, setEnabled ] = useState(false);
    const [showForm, setShowForm ] = useState(false);
    const [ seletedBoard, setSelectedBoard ] = useState(0);
    const [ boardData, setBoardData ] = useState(BoardData);

    useEffect(() => {
        const animation = requestAnimationFrame(() => setEnabled(true));

        return() => {
            cancelAnimationFrame(animation);
            setEnabled(false);
        }
    },[]);

    if(!enabled) {
        return null;
    }

    const onTextAreaKeyPress = (e) => {
        if(e.keyCode ===13){
            const val = e.target.value;
            if (val.length === 0) {
                setShowForm(false);
            } else {
                const boardId = e.target.attributes['data-id'].value;
                const item = {
                    id: createGuidId(),
                    title: val,
                    priority: 0,
                    chat: 0,
                    attachment: 0,
                    assignees: []
                }
                let newBoardData = boardData;
                newBoardData[boardId].items.push(item);
                setBoardData(newBoardData);
                setShowForm(false);
                e.target.value = '';
            }
        }
    }

    return (
        <div className='grid grid-cols-5 gap-5 my-5'> {
            BoardData.map((board, index) => {
                return (
                    <div key={board.seccion}>
                        <Droppable droppableId={ index.toString() } >
                            {
                                (provider, snapshot) => (
                                    <div 
                                        ref={provider.innerRef}
                                        {...provider.droppableProps} >
                                        <div className={`bg-zinc-300 p-2 rounded-sm shadow-md flex flex-col relative overflow-hidden 
                                        ${snapshot.isDraggingOver  && "bg-green-100"}`}>
                                            <span className='w-full h-1 bg-gradient-to-r from-orange-500 to-orange-300 absolute inset-x-0 top-0'></span>
                                            <h4 className='flex justify-between items-center mb-2'>
                                                <span className='text-1xl font-semibold text-gray-800'>{board.seccion}</span>
                                                <EllipsisVerticalIcon className='w-6 h-6 font-semibold text-gray-800'/>
                                            </h4>

                                            <div className=' overflow-y-auto overflow-x-hidden h-auto p-1' style={{maxHeight:'calc(100vh - 290px)'}}>
                                                {
                                                    board.items.length > 0 && (
                                                        board.items.map((item, iIndex) => {
                                                            return (<CardItem key={item.id} data = { item } index={iIndex} />)
                                                        })
                                                    )
                                                }
                                                {provider.placeholder}
                                            </div>
                                            {
                                                showForm && seletedBoard === index ? (
                                                    <div className='p-3 outline-none'>
                                                        <textarea 
                                                            className='border-gray-400 rounded focus:ring-orange-500 w-full'
                                                            placeholder='Task info'
                                                            data-id={index} 
                                                            rows={3}
                                                            onKeyDown={(e) => onTextAreaKeyPress(e)}>
                                                        </textarea>
                                                    </div>
                                                ) : (
                                                    <button 
                                                        type='button' 
                                                        className='flex items-center justify-center my-3 space-x-2 text-lg'
                                                        onClick={ () => {setSelectedBoard(index); setShowForm(true);}}
                                                    >
                                                        <span>Add task</span>
                                                        <PlusCircleIcon className='w-5 h-5 text-gray-500'/>
                                                    </button>
                                                )
                                            }
                                        </div>
                                    </div>
                                )
                            }
                        </Droppable>
                    </div>
                );
            })
        } </div>
    );
}

export default DroppableAnimation;