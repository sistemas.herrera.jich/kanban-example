import React from 'react';
import Image from 'next/image';
import {
    ChatBubbleBottomCenterTextIcon,
    PaperClipIcon,
    PlusIcon
  } from '@heroicons/react/24/outline';
import { Draggable } from 'react-beautiful-dnd';

function CardItem({data, index}) {
    return (
        <Draggable index={index} draggableId={data.id.toString()}>
            {
                (provider) => (
                    <div 
                    ref={provider.innerRef}
                    {...provider.draggableProps}
                    {...provider.dragHandleProps}
                    className=' bg-white rounded-md p-2 mt-3'>
                        <label htmlFor="" className={
                            `
                            bg-gradient-to-r
                            px-2 py-1 rouded text-white text-sm rounded
                            ${data.priority === 0 ? 'from-green-200 to-green-400'
                            : data.priority === 1 ? 'from-orange-200 to-orange-400': 'from-red-300 to-red-500'}
                            `
                        }>
                            { data.priority == 0 ? "Low Priority" : data.priority === 1 ? "Medium Priority" : "High Priority" }
                        </label>
                        <h5 className='text-sm font-semibold text-justify my-2'>{ data.title}</h5>
                        <div className='flex justify-between'>
                            <div className='flex space-x-4 items-center'>
                            <span className='flex space-x-0 items-center'>
                                <ChatBubbleBottomCenterTextIcon className='h-4 w-4 text-gray-400 mr-2' />
                                <span>{data.chat}</span>
                            </span>
                            <span className='flex space-x-0 items-center'>
                            <PaperClipIcon className='h-4 w-4 text-gray-400 mr-2' />
                                <span>{ data.attachment}</span>
                            </span>
                            </div>
                            <ul className='flex space-x-3'>
                                {
                                    data.assignees.map((assig, index) => {
                                        return (
                                            <li key={index}>
                                                <Image 
                                                alt='User random' 
                                                width={"24"} 
                                                height={"24"} 
                                                src={assig.avg} 
                                                className="rounded-full" />
                                            </li>
                                        )
                                    })
                                }
                                <li>
                                    <button type="button" className='flex items-center border border-dashed w-6 h-6 border-gray-500 justify-center rounded-full'>
                                    <PlusIcon className='w-4 h-4 text-gray-500'/>
                                    </button>
                                </li>
                            </ul>
                        </div>
                    </div>
                )
            }
        </Draggable>
     );
}

export default CardItem;