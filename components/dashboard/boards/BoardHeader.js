import React from 'react';
import {
    ChevronDownIcon,
    PlusIcon
} from '@heroicons/react/24/outline';
import Image from 'next/image';

function BoardHeader() {
    return (
          <div className='flex flex-initial justify-between'>
            <div className='flex items-center'>
              <h4 className='text-2xl font-semibold text-gray-600'>
                Proyecto Aperturas
              </h4>
              <ChevronDownIcon className='w-7 h-7 text-gray-600 font-bold rounded-full 
              bg-orange-300 p-1 ml-5 shadow-xl'/>
            </div>
            <div>
              <ul className='flex space-x-3'>
                <li>
                <Image alt='User random' width={"40"} height={"40"} src={"https://randomuser.me/api/portraits/men/30.jpg"} 
                      className="rounded-full" />
                </li>
                <li>
                <Image alt='User random' width={"40"} height={"40"} src={"https://randomuser.me/api/portraits/men/60.jpg"} 
                      className="rounded-full" />
                </li>
                <li>
                <Image alt='User random' width={"40"} height={"40"} src={"https://randomuser.me/api/portraits/men/40.jpg"} 
                      className="rounded-full" />
                </li>
                <li>
                  <button type="button" className='flex items-center border border-dashed w-9 h-9 border-gray-500 justify-center rounded-full'>
                    <PlusIcon className='w-6 h-6 text-gray-500'/>
                  </button>
                </li>
              </ul>
            </div>
          </div>
     );
}

export default BoardHeader;