import React, {useState, useEffect} from 'react';
import { DragDropContext } from "react-beautiful-dnd";
import DroppableAnimation from "./components/Droppable";
import BoardData from '../../../data/board-data.json';

function BoardStudio() {
    const [ ready, setReady ] = useState(false);
    const [ boardData, setBoardData ] = useState(BoardData);

    useEffect(() => {
        if(typeof window !== "undefined"){
            setReady(true)
        }
    },[]);

    const onDragEnd = (re) => {
        if(! re.destination) return;
        
        let newBoardData = boardData;
        let dragItem = newBoardData[parseInt(re.source.droppableId)].items[re.source.index];
        newBoardData[parseInt(re.source.droppableId)].items.splice(re.source.index,1);
        newBoardData[parseInt(re.destination.droppableId)].items.splice(re.destination.index, 0, dragItem);
        setBoardData(newBoardData);
    }

    return (
        <>
            {
                ready && (
                    <DragDropContext onDragEnd={onDragEnd}>
                        <DroppableAnimation/>
                    </DragDropContext>
                )
            }
        </>
     );
}

export default BoardStudio;