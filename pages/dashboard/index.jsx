import React from 'react';
import Layout from '../../components/dashboard/Layout';
import BoardHeader from '../../components/dashboard/boards/BoardHeader';
import BoardStudio from '../../components/dashboard/boards/BoardStudio';

function Dashboard() {
    return ( 
        <Layout>
            <div className='p-10 flex flex-col h-screen'>
                <BoardHeader/>
                <BoardStudio />
            </div>
        </Layout>
     );
}

export default Dashboard;